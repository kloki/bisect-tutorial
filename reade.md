Somehow a bug has ended up in our progame.py. Luckily its self-aware and will
tell you if there is a bug or not. 

$ ./program.py

We want to know which commit causes introduces the error. Sadly the intern's
commits messages arent really helpful

$ git log

We can find the error using git bisect
First start
$ git bisect start
Tell git the current version is bad
$ git bisect bad       
This commit is known to be good (its the first)
$ git bisect good 1d384ac99fa1d69c153b7a77aac3af9d170393c3

Now git has two reference points it will start serving different commits
for you to test. 

Tell git if they are good or bad.
$ git bisect good/bad

repeat.